class Upload < ApplicationRecord
    belongs_to :user
    has_many :groups, through: :file_permissions 
    
    validates(:private, presence:true)
    
    has_attached_file :attachment
    
    validates_attachment_size :attachment, :in => 0..4000.kilobytes 
    
    validates_attachment_content_type :attachment, :content_type => ["application/msword", "application/pdf","text/plain","application/x-compressed","application/excel", /\Aimage\/.*\Z/]
    
    
end
