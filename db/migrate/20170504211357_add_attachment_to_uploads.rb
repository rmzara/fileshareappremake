class AddAttachmentToUploads < ActiveRecord::Migration[5.0]
  def self.up
    add_attachment :uploads, :attachment
  end

  def self.down
    remove_attachment :uploads, :attachment
  end
end
