class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: [:new, :create, :destroy]
  
  def index
    @users = User.paginate(page: params[:page])
    
    respond_to do |f|
      f.js
    end
  end
  
  def show
    @user = User.find(params[:id])
    
    respond_to do |f|
      f.js
    end
  end
  
  def new
    @user = User.new
    
    respond_to do |f|
      f.js
    end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'New user created'
      

      respond_to do |f|
        f.js
      end
      
    else
      respond_to do |f|
        f.js {render 'new'}
      end
    end
    
    
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "account updated"
      
      respond_to do |f|
        f.js
      end
    else
      respond_to do |f|
        f.js {render 'edit'}
      end
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
   
    respond_to do |f|
        f.js {render 'index'}
      end
      
  end
  
  private
      def user_params
        params.require(:user).permit(:name, :email).merge(:password => "testtest", :password_confirmation => "testtest")
      end
      
     
      def correct_user
        @user = User.find(params[:id])
        redirect_to(root_url) unless current_user?(@user)
      end
      
      
end
