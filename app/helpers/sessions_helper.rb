module SessionsHelper
  
  def log_in(user)
    session[:user_id] = user.id
  end
  
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  def logged_in?
    !current_user.nil?  
  end
  
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
  
  def current_user?(user)
    user == current_user
  end
  
  def admin_user
        redirected_to(root_url) unless current_user.admin?
  end
  
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "please log in"
      
      render :js => "window.location = '#{login_url}'"
    end
  end
  
  def redirect_back_or(default)
    ##redirect_to(session[:forwarding_url],  || default, format: 'js')
   
    render js: "window.location = '#{root_path}'"

    session.delete(:forwarding_path)
  end
  
  def store_location
    session[:forwarding_path] = request.original_fullpath if request.get?
  end
  
end
