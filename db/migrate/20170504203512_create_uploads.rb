class CreateUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :uploads do |t|
      t.belongs_to :user, index: true
      t.boolean :private
      t.timestamps
    end
  end
end
