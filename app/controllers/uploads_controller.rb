class UploadsController < ApplicationController
  before_action :logged_in_user
  
  def index
    respond_to do |f|
      f.js
    end
  end
  
  
  def index
    if(current_user.admin?)
      @uploads = Upload.paginate(page: params[:page])
    
    
    else
      @uploads = Upload.where(private: false)
      
      @uploads = @uploads.join(current_user.uploads)
      
      current_user.groups.each do |group|
       @uploads = @uploads.joins(group.uploads)
       
       
      end
      
      @uploads = @uploads.paginate(page: params[:page])
      
      respond_to do |f|
        f.js
      end
    end
  
  end 

  
  def show
    @upload = Upload.find(params[:id])
    send_file(Paperclip.io_adapters.for(@upload.attachment).path, :type => @upload.attachment_content_type, :disposition => "attachment", :filename => @upload.attachment_file_name)
    render js: "window.location = '#{uploads_path}'"
  end
  
  def new
    @upload = Upload.new
    respond_to do |f|
      f.js
    end
  end 
      
  def create
    @upload = Upload.new(upload_params.merge(user_id: current_user.id))
    if @upload.save
      respond_to do |f|
      f.js
    end
    else
      render action: 'new'
    end
  end
  
  def edit
  
  end

  def update
  
  def destroy

  end

end
    private

    def upload_params
      params.require(:upload).permit(:attachment, :private)
    end
    
    
end
