class GroupsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: [:edit, :update, :destroy]
 
 
  def index
    if(current_user.admin?)
      @groups = Group.paginate(page: params[:page])
 
    else
      @groups = current_user.groups.paginate(page: params[:page])
    
    end
    
    respond_to do |f|
      f.js
    end
  end
  
  
  def show
    @group = Group.find(params[:id])
    @members = @group.users.paginate(page: params[:page])
    
    respond_to do |f|
      f.js
    end
  end
  
  def new
    @group = Group.new
    
    respond_to do |f|
      f.js
    end
  end
  
  def create
    @group = Group.new(group_params)
    if @group.save
       @group.group_memberships.create(user: current_user, owner: true, permission_share_files: true,permission_invite_users: true)
        
       flash[:success] = 'New group created'
       respond_to do |f|
        f.js 
       end
       
    else
      respond_to do |f|
        f.js {render 'new'}
      end
    end
  end
  
  def edit
    @group = Group.find(params[:id])
    
    
    
    respond_to do |f|
      f.js
    end
  end
  
  def update
    respond_to do |f|
      f.js
    end
    
    @group = Group.find(params[:id])
    if @group.update_attributes(group_params)
      flash[:success] = "group updated"
      redirect_to @group
    else
      render 'edit'
    end
  end
  
  def destroy
    Group.find(params[:id]).destroy
    flash[:success] = "Group deleted"
    
    respond_to do |f|
        f.js 
    end
    
  end
  
  private
      def group_params
        params.require(:group).permit(:name)
      end
      
      def logged_in_user
        unless logged_in?
          store_location
          flash[:danger] = "please log in"
          redirect_to login_url
        end
      end
      
      def correct_user
        u_id = group_owner_id
        @user = User.find(u_id)
        redirect_to(root_url) unless current_user?(@user)
      end
      
      def admin_user
        redirected_to(root_url) unless current_user.admin?
      end

      def group_owner_id
       @group = Group.find(params[:id])
       g_id = @group.id
       u_id_array = @group.group_memberships.where(owner: true).pluck(:user_id)
       u_id = u_id_array[0] 
       return u_id
    end
end
