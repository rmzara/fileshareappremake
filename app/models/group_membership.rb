class GroupMembership < ApplicationRecord
    belongs_to :user
    belongs_to :group
    
    validates(:owner, presence:true)
    validates(:permission_share_files, presence:true)
    validates(:permission_invite_users, presence:true)
end
