class StaticPagesController < ApplicationController
  before_action :logged_in_user, only: [:management]
  before_action :admin_user, only: [:management]
  
  def home
  end

  def help
  end
  
  def about
  end

  def contact
  end
  
  def management
    respond_to do |f|
      f.js
    end
  end
  
  
end
