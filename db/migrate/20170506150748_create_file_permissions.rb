class CreateFilePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :file_permissions do |t|
      t.belongs_to :group, index: true
      t.belongs_to :upload, index: true
      t.timestamps
    end
  end
end
