class CreateGroupMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :group_memberships do |t|
      t.belongs_to :group, index: true
      t.belongs_to :user, index: true
      t.boolean :owner
      t.boolean :permission_share_files
      t.boolean :permission_invite_users
      t.timestamps
    end
  end
end
