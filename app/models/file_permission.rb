class FilePermission < ApplicationRecord
  belongs_to :upload
  belongs_to :group
end
