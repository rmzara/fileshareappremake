class Group < ApplicationRecord
    has_many :users, through: :group_memberships
    has_many :group_memberships
    has_many :uploads, through: :file_permissions
    
    validates(:name, presence:true, length: {maximum: 50})
    self.per_page = 10
end
