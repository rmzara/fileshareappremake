module GroupsHelper
    def group_owner(group)
        g_id = group.id
        u_id_array = group.group_memberships.where(owner: true).pluck(:user_id)
        u_id = u_id_array[0]
        u_name = User.find(u_id).name
        return u_name
    end   
    
    def group_owner_id(group)
       g_id = group.id
       u_id_array = group.group_memberships.where(owner: true).pluck(:user_id)
       u_id = u_id_array[0] 
       return u_id
    end
    
    def group_permissions(group)
        
    end
    
    def current_user_is_member?(group)
        group.users.pluck(:name)
    end
    
    def current_user_permission_check(group)
        
            g_mems = group.group_memberships
            u_id_arr = g_mems.where(owner: true).or(g_mems.where(permission_share_files: true)).pluck(:user_id)
            return u_id_arr.include?(current_user.id)
                
    end
end
