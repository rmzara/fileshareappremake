u = User.create!(name:  "Test User",
             email: "tester@teamone.com",
             password:              "testtest",
             password_confirmation: "testtest",
             admin: true)


fg = Group.create!(name:  "Test Group"
             )
fg.group_memberships.create(user: u,owner: true,permission_share_files: true,permission_invite_users: true)



99.times do |n|
  name  = Faker::Name.name
  email = "tester-#{n+1}@teamone.com"
  password = "password"
  tu = User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)

  nameg = "testgroup-#{n+1}"
  g = Group.create!(name:  nameg
               )
               
  g.group_memberships.create(user: tu,owner: true,permission_share_files: true,permission_invite_users: true)             
end


