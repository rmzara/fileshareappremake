class User < ApplicationRecord
    has_many :groups, through: :group_memberships
    has_many :group_memberships
    has_many :uploads
    
    
    
    before_save{self.email = email.downcase}
    validates(:name, presence:true, length: {maximum: 50})
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates(:email, presence:true, length: {maximum: 255},
                      format: {with: VALID_EMAIL_REGEX},
                      uniqueness: {case_sensitive: false})
                      
                      
    has_secure_password                  
    validates(:password, presence: true, length: {minimum:6}, allow_nil: true)                  

    self.per_page = 10
end
